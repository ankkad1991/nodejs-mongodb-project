FROM node:10
WORKDIR /test
COPY package.json /test
RUN npm install
COPY . /test
CMD node node.js
EXPOSE 9000

FROM jenkins
WORKDIR /test
COPY package.json /test
COPY . /test
EXPOSE 8080